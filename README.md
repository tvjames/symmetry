# Symmetry

Configuration management library designed to be a seamless addition to your .net project. 

## Overview

Provides configuration override options to allow easy dev -> test -> prod deployment without needing to manually
edit files on each deployment. 

Just setup your default values in your App/Web.config then provide a series of override files that when present
will replace the values of the file.
