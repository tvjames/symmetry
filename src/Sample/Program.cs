﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sample
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;

    using Symmetry;

    public class Program
    {
        public static void Main(string[] args)
        {
            DisplayAppSettings();

            var apply =
                new Func<string, Action<AppConfigConfigurationSource>, Action<AppConfigConfigurationSource>>(
                    (m, a) => x =>
                        {
                            Console.WriteLine("[{0}] Apply", m);
                            a(x);
                        });

            var current = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var config = new Config(current);
            var configurationCollection = new ConfigurationCollection
                {
                    new ConfigurationCollectionOption
                        {
                            ConfigurationSource = new AppConfigConfigurationSource("Merge.config"),
                            Apply = apply("Merge.config", x => config.Merge(x))
                        },
                    new ConfigurationCollectionOption
                        {
                            ConfigurationSource = new AppConfigConfigurationSource("Local.config"),
                            Apply = apply("Local.config", config.ImportIntoRunningConfig)
                        },
                    new ConfigurationCollectionOption
                        {
                            ConfigurationSource = new AppConfigConfigurationSource("Missing.config"),
                            Apply = apply("Missing.config", config.ImportIntoRunningConfig)
                        },
                };
            configurationCollection.ConfigureAndWatch();

            ConfigurationManager.AppSettings["sample"] = "added in proc";

            Console.WriteLine("After merge");
            DisplayAppSettings();

            Console.ReadLine();
        }

        private static void DisplayAppSettings()
        {
            foreach (var key in ConfigurationManager.AppSettings.AllKeys)
            {
                Console.WriteLine("AppSetting [key={0}] [value={1}]", key, ConfigurationManager.AppSettings[key]);
            }

            for (var i = 0; i < ConfigurationManager.ConnectionStrings.Count; i++)
            {
                var cs = ConfigurationManager.ConnectionStrings[i];
                Console.WriteLine("ConnectionStrings [name={0}] [provider={1}]", cs.Name, cs.ProviderName);
            }
        }
    }
}
