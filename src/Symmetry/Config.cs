// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Config.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the Config type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Symmetry
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;

    /*
     * ConfigMerge

    Could also include a "fail fast" style configuration validation. 
    Allow defaults, but also require certain entries to be supplied. 
    
    e.g.
    
    ConfigMerge.Configure(...)
        .Require(ConnectionString.Name, "name value", x => string.Format("Missing required entry for {0}", x))
        .Require(AppSetting.Name, "key name")
        .Validate(AppSetting.Name, "key name", (k, v) => !string.IsNullOrWhitespace(v), (k, v) => string.Format("{0} must not contain an empty value", k))
        .Validate(...)
     * */

    public class Config
    {
        private readonly Configuration configuration;

        public Config(Configuration configuration)
        {
            this.configuration = configuration;
        }

        public void Merge(params AppConfigConfigurationSource[] source)
        {
            var tracker = new ConfigurationChangedTracker();
            Array.ForEach(source, x => x.ApplyTo(this.configuration, tracker));
            this.configuration.Save();
            tracker.Reload();
        }

        public void ImportIntoRunningConfig(IConfigurationSource configurationSource)
        {
            var field = typeof(ConfigurationElementCollection)
                .GetField("bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            Debug.Assert(field != null, "ConfigurationElementCollection.bReadOnly != null");
            field.SetValue(ConfigurationManager.ConnectionStrings, false);

            configurationSource.ApplyTo(c =>
                {
                    var appSettings = c.AppSettings.Settings;
                    foreach (var key in appSettings.AllKeys)
                    {
                        ConfigurationManager.AppSettings[key] = appSettings[key].Value;
                    }

                    var connectionStrings = c.ConnectionStrings.ConnectionStrings;
                    for (var i = 0; i < connectionStrings.Count; i++)
                    {
                        var cs = connectionStrings[i];
                        if (cs.ElementInformation.Source == null)
                        {
                            continue;
                        }

                        var existing = ConfigurationManager.ConnectionStrings[cs.Name];
                        if (existing != null)
                        {
                            ConfigurationManager.ConnectionStrings.Remove(existing.Name);
                        }

                        ConfigurationManager.ConnectionStrings.Add(new ConnectionStringSettings(cs.Name, cs.ConnectionString, cs.ProviderName));
                    }
                });
        }
    }
}