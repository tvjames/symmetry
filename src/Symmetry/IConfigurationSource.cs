namespace Symmetry
{
    using System;
    using System.Configuration;

    public interface IConfigurationSource
    {
        void ApplyTo(Configuration currentConfiguration, ConfigurationChangedTracker tracker);

        void ApplyTo(Action<Configuration> currentConfiguration);
    }
}