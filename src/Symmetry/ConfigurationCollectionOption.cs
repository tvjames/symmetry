// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationCollectionOption.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the ConfigurationCollectionOption type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Symmetry
{
    using System;

    public class ConfigurationCollectionOption
    {
        public AppConfigConfigurationSource ConfigurationSource { get; set; }

        public Action<AppConfigConfigurationSource> Apply { get; set; }
    }
}