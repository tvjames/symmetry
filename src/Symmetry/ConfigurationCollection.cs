// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationCollection.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the ConfigurationCollection type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Symmetry
{
    using System.Collections;
    using System.Collections.Generic;

    public class ConfigurationCollection : IEnumerable<ConfigurationCollectionOption>
    {
        private readonly List<ConfigurationCollectionOption> options = new List<ConfigurationCollectionOption>();

        public void Add(ConfigurationCollectionOption option)
        {
            this.options.Add(option);
        }

        public IEnumerator<ConfigurationCollectionOption> GetEnumerator()
        {
            return this.options.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void ConfigureAndWatch()
        {
            this.Configure();
            this.options.ForEach(x => x.ConfigurationSource.WhenChanged(this.Configure));
        }

        public void Configure()
        {
            this.options.ForEach(x => x.Apply(x.ConfigurationSource));
        }
    }
}