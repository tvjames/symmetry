﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppConfigConfigurationSource.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the AppConfigConfigurationSource type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Symmetry
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class AppConfigConfigurationSource : IConfigurationSource
    {
        private readonly ExeConfigurationFileMap configMap;

        private readonly FileSystemWatcher watcher = new FileSystemWatcher();

        public AppConfigConfigurationSource(string path)
        {
            this.configMap = new ExeConfigurationFileMap { ExeConfigFilename = path, };

            var file = new FileInfo(path);
            this.watcher = new FileSystemWatcher(file.DirectoryName ?? string.Empty, file.Name)
                {
                    NotifyFilter =
                        NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName
                        | NotifyFilters.DirectoryName,
                    EnableRaisingEvents = true
                };
        }

        public void ApplyTo(Configuration currentConfiguration, ConfigurationChangedTracker tracker)
        {
            var configuration = this.ReadConfiguration();
            var appSettingsSection = (AppSettingsSection)currentConfiguration.GetSection("appSettings");
            if (appSettingsSection != null)
            {
                foreach (var key in configuration.AppSettings.Settings.AllKeys)
                {
                    var config = configuration.AppSettings.Settings[key];
                    Console.WriteLine(
                        "[AppConfigConfigurationSource] {0}={1}", key, configuration.AppSettings.Settings[key].Value);

                    if (appSettingsSection.Settings.AllKeys.Contains(key))
                    {
                        appSettingsSection.Settings[key].Value = config.Value;
                    }
                    else
                    {
                        appSettingsSection.Settings.Add(config);
                    }
                }

                tracker.Changed(configuration.AppSettings.SectionInformation.SectionName);
            }

            var connectionStringsSection = (ConnectionStringsSection)currentConfiguration.GetSection("connectionStrings");
            if (connectionStringsSection != null)
            {
                for (var i = 0; i < configuration.ConnectionStrings.ConnectionStrings.Count; i++)
                {
                    var connectionString = configuration.ConnectionStrings.ConnectionStrings[i];
                    Console.WriteLine(
                        "[AppConfigConfigurationSource] {0}={1}", connectionString.Name, connectionString.ProviderName);

                    var existing = connectionStringsSection.ConnectionStrings[connectionString.Name];
                    if (existing == null)
                    {
                        connectionStringsSection.ConnectionStrings.Add(new ConnectionStringSettings(connectionString.Name, connectionString.ConnectionString, connectionString.ProviderName));
                    }
                    else
                    {
                        existing.ProviderName = connectionString.ProviderName;
                        existing.ConnectionString = connectionString.ConnectionString;
                    }
                }

                tracker.Changed(configuration.ConnectionStrings.SectionInformation.SectionName);
            }
        }

        public void ApplyTo(Action<Configuration> currentConfiguration)
        {
            var configuration = this.ReadConfiguration();
            currentConfiguration(configuration);
        }

        public void WhenChanged(Action action)
        {
            this.watcher.Changed += (sender, args) => action();
        }

        private Configuration ReadConfiguration()
        {
            var configuration = ConfigurationManager.OpenMappedExeConfiguration(this.configMap, ConfigurationUserLevel.None);
            return configuration;
        }
    }
}
