// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationChangedTracker.cs" company="Red Ink Labs">
//   Thomas James 2012
// </copyright>
// <summary>
//   Defines the ConfigurationChangedTracker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Symmetry
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    public class ConfigurationChangedTracker
    {
        private readonly List<string> sections = new List<string>();

        public void Reload()
        {
            foreach (var section in this.sections)
            {
                ConfigurationManager.RefreshSection(section);
            }
        }

        public void Changed(string sectionName)
        {
            this.sections.Add(sectionName);

            Console.WriteLine("[ConfigurationChangedTracker] Changed: {0}", sectionName);
        }
    }
}