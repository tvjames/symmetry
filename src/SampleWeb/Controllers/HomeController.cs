﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleWeb.Controllers
{
    using System.Configuration;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            ViewBag.Setting = ConfigurationManager.AppSettings["SomeSetting"];

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
